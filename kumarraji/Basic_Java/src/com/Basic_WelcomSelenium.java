package com;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Basic_WelcomSelenium {

	public static void main(String[] args) {
		/*
		 * System.setProperty("webdriver.gecko.driver",
		 * "C:\\Users\\user\\eclipse-workspace\\Basic_Java\\jars\\geckodriver.exe"); //
		 * Your gecko_driver path. WebDriver driver = new FirefoxDriver();
		 */
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\eclipse-workspace\\Basic_Java\\jars\\chromedriver.exe");   // Your gecko_driver path.
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com");
		driver.findElement(By.name("email")).sendKeys("Username");
	}

}
